﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/TextureShader" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex("Diffuse Textur", 2D) = "white" {}
	}
	SubShader {
	Pass{

		CGPROGRAM
		#pragma vertex vertexFunction
		#pragma fragment fragmentFunction

		//user defined variables
		uniform float4 _Color;
		uniform sampler2D _MainTex;
		uniform float4 _MainTex_ST;

		//unity defined variables

		//input struct
		struct inputStruct 
		{
			float4 vertexPos : POSITION;
			float4 textureCoord : TEXCOORD0;
		};

		//output struct ?
		struct outputStruct
		{
		    float4 pixelPos: SV_POSITION;
		    float4 tex: TEXCOORD0;
		};

		//vertex program
		outputStruct vertexFunction(inputStruct input)
		{
			   outputStruct toReturn;
			   toReturn.pixelPos = UnityObjectToClipPos(input.vertexPos);

			   //Assigning the information that the inputStruct gathered
			   //to the struct that will be passed into the fragment
			   //program so that the fragmetn program can use it.
			   toReturn.tex = input.textureCoord;
			   return toReturn;
		}

		//fragmentprogram
		float4 fragmentFunction(outputStruct input) : COLOR
		{
			   float2 placeHolder = _MainTex_ST.xy;
			   _MainTex_ST.xy = _MainTex_ST.zw;
			   _MainTex_ST.zw = placeHolder;
			   float4 tex = tex2D(_MainTex, _MainTex_ST.xy * input.tex.xy + _MainTex_ST.zw);
			   return _Color * tex;
		}


		ENDCG
		}
	}
	//Fallback
	//FallBack "Diffuse"
}
